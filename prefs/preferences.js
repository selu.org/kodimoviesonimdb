const form = document.getElementById('preferences-form');
const warning = document.getElementById('warning');
const btn = form.querySelector('button');
const upload = document.getElementById('datafile');
const download = document.getElementById('download');

form.addEventListener('submit', e => {
  e.preventDefault();
  const data = Array.prototype.reduce.call(form.elements, (obj, el) => {
    obj[el.name] = el.value;
    return obj;
  }, {});
  delete data[''];

  if (!data.rpc_url.endsWith('/jsonrpc')) {
    warning.innerHTML = 'WARNING: Server URL does not end with /jsonrpc - make sure it is correct!';
    warning.hidden = false;
  } else {
    warning.hidden = true;
  }

  browser.storage.local.set({server: data}).then(() => {
    btn.innerHTML = 'Saved!';
    setTimeout(() => {
      btn.innerHTML= 'Save';
    }, 600);
  });
});

upload.addEventListener('change', e => {
  e.preventDefault();
  var reader = new FileReader();
  reader.onload = _done => {
    browser.runtime.sendMessage({msg: "upload", data: reader.result});
  };
  reader.readAsText(e.target.files[0]);
});

download.addEventListener('click', e => {
  e.preventDefault();
  browser.runtime.sendMessage({msg: "download"});
});

browser.storage.local.get('server').then(({server}) => {
  if (!server) {
    return;
  }

  for (const key of Object.keys(server)) {
    const el = form.querySelector('[name="' + key + '"]');
    if (el) {
      el.value = server[key];
    }
  }
});
