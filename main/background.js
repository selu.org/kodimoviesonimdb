
var mymovies = null;

function haveThisMovie(moviecode) {
  if (mymovies) {
    return {
      have: moviecode in mymovies,
      played: moviecode in mymovies && mymovies[moviecode].cnt > 0,
      ids: moviecode in mymovies
          ? mymovies[moviecode].ids
          : null
    };
  } else {
    return {have: false, played: false, ids: null};
  }
}

browser.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (message.msg == "movie") {
    sendResponse(haveThisMovie(message.code));
  } else if (message.msg == "reload") {
    refreshMyMovies();
  } else if (message.msg == "download") {
    downloadMyMovies();
  } else if (message.msg == "upload") {
    uploadMyMovies(message.data);
  } else {
    console.warn(`Unknown message: ${message.msg}`);
  }
});

function refreshPages() {
  browser.tabs.query({url:"*://*.imdb.com/*"}).then((tabs) => {
    for (let tab of tabs) {
      browser.tabs.sendMessage(tab.id, {message: "refresh"});
      var moviecode = tab.url.match(/tt\d+/);
      setPageAction(tab.id, moviecode);
    }
  });
}

function handleMovieList(response) {
  var temp = {};
  response.result.movies.forEach(function(item) {
    if (item.imdbnumber in temp) {
      temp[item.imdbnumber].cnt += item.playcount
    } else {
      temp[item.imdbnumber] = {cnt: item.playcount, ids: {}};
    }
    temp[item.imdbnumber].ids[item.movieid] = item.file
  });
  mymovies = temp;
  refreshPages();
}

function jsonrpc(method, params, handler) {
  browser.storage.local.get('server').then(({server}) => {
    if (server.rpc_url == '') {
      return;
    }
    var headers = new Headers();
    headers.append('Content-Type','application/json');
    if (server.username !== '') {
      headers.append('Authorization', 'Basic '+btoa(server.username+':'+server.password));
    }
    var data = {
      id: 1,
      jsonrpc: "2.0",
      method: method,
      params: params
    };
    var request = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: headers,
      credentials: 'include',
    };
    fetch(server.rpc_url, request)
    .then(function(resp) {
      return resp.json();
    })
    .then(handler)
    .catch(function(err){
      console.error(`jsonrpc error (${method}): ${err}`);
    });
  });
}

function refreshMyMovies() {
  jsonrpc(
    "VideoLibrary.GetMovies",
    {
      properties: [
        "imdbnumber",
        "playcount",
        "file"
      ]
    },
    handleMovieList
  )
}

browser.storage.onChanged.addListener((changes, _area) => {
  if ('server' in changes) {
    const oldv = changes.server.oldValue;
    const newv = changes.server.newValue;
    if (!oldv || oldv.rpc_url !== newv.rpc_url ||
        oldv.username !== newv.username || oldv.password !== newv.password) {
      refreshMyMovies();
    }
  } else if ('mymovies' in changes) {
    mymovies = changes.mymovies.newValue;
  }
});

function setPageAction(tabId, moviecode) {
  if (moviecode) {
    var response = haveThisMovie(moviecode[0]);
    if (response.have) {
      if (response.played) {
        browser.pageAction.setIcon({
          tabId: tabId, path: "icons/page_seen.svg"
        });
        browser.pageAction.setTitle({
          tabId: tabId, title: browser.i18n.getMessage("pageTitleMovieSeen")
        })
      } else {
        browser.pageAction.setIcon({
          tabId: tabId, path: "icons/page_have.svg"
        });
        browser.pageAction.setTitle({
          tabId: tabId, title: browser.i18n.getMessage("pageTitleMovieHave")
        })
      }
    }
  }
}

browser.tabs.onUpdated.addListener((tabId, changeInfo, _tabInfo) => {
  if (changeInfo.url && changeInfo.url.includes("imdb.com/title/tt")) {
    var moviecode = changeInfo.url.match(/tt\d+/);
    setPageAction(tabId, moviecode);
  }
});

function onCreated() {
  if (browser.runtime.lastError) {
    console.warn(`Error: ${browser.runtime.lastError}`);
  }
}

browser.menus.create({
  id: "play-movie",
  title: browser.i18n.getMessage("menuItemPlayMovie"),
  contexts: ["link"],
  documentUrlPatterns: ["*://*.imdb.com/*"],
  targetUrlPatterns: ["*://*.imdb.com/title/tt*"]
}, onCreated);

browser.menus.onShown.addListener(info => {
  if (info.menuIds.includes("play-movie")) {
    var moviecode = info.linkUrl.match(/tt\d+(?=\/?(?:\?.*)$)/);
    var haveit = moviecode && moviecode[0] in mymovies;
    browser.menus.update("play-movie", {enabled: haveit}).then(() => {
      browser.menus.refresh();
    }, () => {
      console.warn("menu item update error");
    });
  }
});

function handlePlayerOpen(_response) {
  // Do nothing
}

browser.menus.onClicked.addListener((info, _tab) => {
  var moviecode = info.linkUrl.match(/tt\d+(?=\/?(?:\?.*)?$)/);
  if (moviecode && moviecode[0] in mymovies) {
    jsonrpc(
      "Player.Open",
      {
        item: {movieid: Number(Object.keys(mymovies[moviecode[0]].ids)[0])}
      },
      handlePlayerOpen
    )
  }
});

var dlUrl = null;
var dlId = null;

browser.downloads.onChanged.addListener(delta => {
  if (delta.state && delta.state.current == 'complete') {
    if (delta.id == dlId && delta.url) {
      URL.revokeObjectURL(delta.url);
    }
  }
});

function downloadMyMovies() {
  dlUrl = URL.createObjectURL(new Blob([JSON.stringify(mymovies)], {type: 'application/json'}));
  browser.downloads.download({
    url: dlUrl,
    filename: 'mymovies.json'
  }).then(id => {
    dlId = id;
  }, error => {
    console.warn(`Download failed: ${error}`);
  });
}

function uploadMyMovies(data) {
  mymovies = JSON.parse(data);
  refreshPages();
}

refreshMyMovies();
