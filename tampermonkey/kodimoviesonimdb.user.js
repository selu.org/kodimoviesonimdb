// ==UserScript==
// @name         My Kodi Movies on IMDB
// @namespace    http://selu.dev/
// @version      0.1
// @description  Highlight my movies from Kodi on IMDB site
// @author       selu
// @match        http*://*.imdb.com/*
// @connect      *
// @require      https://openuserjs.org/src/libs/sizzle/GM_config.js
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_addStyle
// @grant        GM_registerMenuCommand
// @grant        GM_xmlhttpRequest
// @updateURL    https://gitlab.com/selu/kodimoviesonimdb/-/raw/master/tampermonkey/kodimoviesonimdb.user.js
// @downloadURL  https://gitlab.com/selu/kodimoviesonimdb/-/raw/master/tampermonkey/kodimoviesonimdb.user.js
// ==/UserScript==

(function() {
    'use strict';

    GM_addStyle(`
        a.mymovie { color: #206030; background-color: #fff0c0; }
        a.mymovie.played { background-color: #c0fff0; }
    `);

    var rcode = /tt\d+(?=\/?(?:\?.*)?$)/;
    var mymovies = null;

    GM_registerMenuCommand('Open Settings', function() {
        GM_config.open();
    });

    function jsonrpc(method, params, handler) {
        var rpc_url = GM_config.get('rpcurl');
        if (rpc_url == '') {
            return;
        }
        var username = GM_config.get('rpcuser');
        var password = GM_config.get('rpcpass');
        var data = {
            id: 1,
            jsonrpc: "2.0",
            method: method,
            params: params
        };
        GM_xmlhttpRequest({
            method: 'POST',
            url: rpc_url,
            data: JSON.stringify(data),
            overrideMimeType: 'application/json',
            responseType: 'json',
            headers: {'Content-Type': 'application/json'},
            username: username,
            password: password,
            onload: function(resp) {
                console.log('jsonrpc status: '+resp.status);
                return handler(resp.response);
            },
            onerror: function(err) {
                console.error(`jsonrpc error (${method}): ${err}`);
            }
        });
    }

    function handleMovieList(response) {
        var temp = {};
        response.result.movies.forEach(function(item) {
            if (item.imdbnumber in temp) {
                temp[item.imdbnumber].cnt += item.playcount
            } else {
                temp[item.imdbnumber] = {cnt: item.playcount, ids: {}};
            }
            temp[item.imdbnumber].ids[item.movieid] = item.file
        });
        mymovies = temp;
        GM_setValue('mymovies', mymovies);
        refreshHighlights();
    }

    function refreshMyMovies() {
        jsonrpc(
            "VideoLibrary.GetMovies",
            {
                properties: [
                    "imdbnumber",
                    "playcount",
                    "file"
                ]
            },
            handleMovieList
        )
    }

    function haveThisMovie(moviecode) {
        if (mymovies) {
            return {
                have: moviecode in mymovies,
                played: moviecode in mymovies && mymovies[moviecode].cnt > 0,
                ids: moviecode in mymovies
                ? mymovies[moviecode].ids
                : null
            };
        } else {
            return {have: false, played: false, ids: null};
        }
    }

    GM_config.init({
        'id': 'MyKodionIMDB',
        'title': 'Kodi Movies on IMDB Setting',
        'css': `
              .section_header { margin: 18px 0 6px !important; }
              label { width: 200px; display:inline-block; padding-left: 24px }
`,
        'events': {
            'open': function(doc) {
                doc.querySelectorAll("input[id$='pass']").forEach(function (inp) { inp.type = 'password'})
            },
            'save': function() {
                refreshMyMovies();
            }
        },
        'fields':
        {
            'rpcurl':
            {
                'section': 'Kodi Server',
                'label': 'Server URL <small>(.../jsonrpc)</small>',
                'labelPos': 'left',
                'type': 'text',
                'size': 80
            },
            'rpcuser':
            {
                'label': 'Username <small>(optional)</small>',
                'type': 'text'
            },
            'rpcpass':
            {
                'label': 'Password <small>(optional)</small>',
                'type': 'password'
            },
            'webdavurl':
            {
                'section': 'WebDAV Backup',
                'label': 'WebDAV URL',
                'labelPos': 'left',
                'type': 'text',
                'size': 80
            },
            'webdavuser':
            {
                'label': 'Username',
                'type': 'text'
            },
            'webdavpass':
            {
                'label': 'Password',
                'type': 'password'
            }
        }
    });

    function refreshHighlights() {
        document.querySelectorAll("a[href^='/title/tt']").forEach((element) => {
            var moviecode = element.href.match(rcode);
            if (moviecode) {
                var result = haveThisMovie(moviecode[0]);
                if (result.have) {
                    element.classList.add('mymovie');
                    if (result.played) {
                        element.classList.add('played');
                    } else {
                        element.classList.remove('played');
                    }
                } else {
                    element.classList.remove('mymovie', 'played');
                }
            }
        });
    }

    if (mymovies == null) {
        mymovies = GM_getValue('mymovies', null);
        if (mymovies == null) {
            refreshMyMovies();
        }
    }
    if (mymovies != null) {
        refreshHighlights();
    }
})();
