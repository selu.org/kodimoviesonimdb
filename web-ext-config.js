/* global module */
module.exports = {
  verbose: true,
  build: {
    overwriteDest: true,
  },
  ignoreFiles: [
    'draw/',
    'web-ext-config.js',
  ],
};
